# -*- coding: utf-8 -*-
__author__ = 'Ann'
import math


def is_prime(n):
    if n % 2 == 0 and n > 2:
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True
print "is_prime(239) - " + str(is_prime(239))




def get_happiness(n):
    str_n = str(n)
    total_happiness = 0
    for c in str_n:
        total_happiness += int(math.pow(int(c), 2))
    return total_happiness
def is_happy(n):
    while n not in range(1, 9):
        n = get_happiness(n)
    if n == 1:
        return True
    else:
        return False
print "is_happy(19) - " + str(is_happy(19))




def is_triangular(n):
    new_n = n
    for i in range(1, n):
        new_n -= i
        if new_n <= 0:
            break
    return True if new_n == 0 else False
print "is_triangular(6) - " + str(is_triangular(6))




def is_square(n):
    new_n = n
    for i in range(1, n, 2):
        new_n -= i
        if new_n <= 0:
            break
    return True if (new_n == 0) or (n == 1) else False
print "is_square(9) - " + str(is_square(9))




def is_smug(n):
    for i in range(1, int(math.sqrt(n)) + 1):
        for j in range(1, int(math.sqrt(n)) + 1):
            if n == i * i + j * j:
                return True
    return False
print "is_smug(5) - " + str(is_smug(5))




def is_honest(n):
    for i in range(1, n):
        if (n / i == i) and (i * i != n):
            return False
    return True
print "is_honest(17) - " + str(is_honest(17))